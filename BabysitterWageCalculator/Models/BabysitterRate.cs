﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BabysitterWageCalculator.Models
{
    public class BabysitterRate
    {
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public TimeSpan? BedTime { get; set; }

        /// <summary>
        /// Calculate the total nigthly charge for the babysitting session
        /// </summary>
        /// <returns></returns>
        public decimal CalculateCharge()
        {
            /*
            * $12/hour from start-time to bedtime
            * $8/hour from bedtime to midnight
            * $16/hour from midnight to end of job
            */
            List<decimal> charges = new List<decimal>();
            TimeSpan hours1, hours2, hours3, hours4;
            
            // Calculate hours between start and bed time at $12 per hour, round to nearest dollar, save charge
            hours1 = ((TimeSpan)BedTime).Subtract((TimeSpan)StartTime);
            charges.Add(Math.Round(Convert.ToDecimal(hours1.TotalHours * 12)));

            // If end time is midnight or after, Calculate hours between bed time to midnight at $8 per hour, calculate hours from midnight to end of job at $16 per hour, round to nearest dollar, save charge
            if (EndTime > new TimeSpan(00, 00, 00) && EndTime <= new TimeSpan(04, 00, 00))
            {
                // Bed time to midnight
                hours2 = new TimeSpan(23, 59, 59).Subtract((TimeSpan)BedTime);
                charges.Add(Math.Round(Convert.ToDecimal(hours2.TotalHours * 8)));

                // Midnight to end of job
                hours3 = ((TimeSpan)EndTime).Subtract(new TimeSpan(00, 00, 00));
                charges.Add(Math.Round(Convert.ToDecimal(hours3.TotalHours * 16)));
            }
            // If end time before midnight, calculate hours between bedtime and midnight at $8 per hour
            else if (EndTime < new TimeSpan(23, 59, 59) || EndTime == new TimeSpan(00,00,00))
            {
                if (EndTime == new TimeSpan(00, 00, 00))
                {
                    // Timespans get weird, being 24 hour clocks, so just just assume 1s before midnight is close enough to midnight
                    hours4 = new TimeSpan(23, 59, 59).Subtract((TimeSpan)BedTime);
                }
                else
                {
                    hours4 = ((TimeSpan)EndTime).Subtract((TimeSpan)BedTime);
                }
                charges.Add(Math.Round(Convert.ToDecimal(hours4.TotalHours * 8)));
            }
            return charges.Sum();
        }
    }
}
