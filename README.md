# Babysitter

Small web application to calculate hourly wage for babysitter. Given to me as a code exercise from a potential employment opportunity.

# User story
As a babysitter
I want to calculate my nightly charge
So that I get paid for 1 night of work

The babysitter:
starts no earlier than 5:00PM
leaves no later than 4:00AM
gets paid for full hours (no fractional hours)
should be prevented from mistakes when entering times (e.g. end time before start time, or outside of allowable work hours)

The job:
- gets paid $12/hour from start-time to bedtime
- gets paid $8/hour from bedtime to midnight
- gets paid $16/hour from midnight to end of job
- gets paid for full hours (no fractional hours)

# Design
- App built with Blazor on .NET 5
- App deployed on Azure websites at https://babysitterwage.azurewebsites.net/

# Resources
- https://mudblazor.com/
- https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-5.0

# Author
- Brett Steffenhagen